from rockem.slave.slave import BaseSlave

class WindowsSlave(BaseSlave):
    def __init__(self, name, password):
        BaseSlave.__init__(self, name, password)
        self.properties['os'] = 'windows'

    def setVisualStudio(self, vs_edition, installdir = None):
        self.setCapability(vs_edition, None, installdir)
        return self

