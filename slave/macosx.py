from rockem.slave.slave import BaseSlave

class MacSlave(BaseSlave):
    def __init__(self, name, password):
        BaseSlave.__init__(self, name, password)
        self.properties['os'] = 'osx'

    def setXcode(self, version = None, installdir = None):
        self.setCapability('xcode', version, installdir)
        return self

