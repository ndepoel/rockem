from rockem import util
from rockem.tool import Capabilities

from rockem.mixin.ios import IOSToolsMixin
from rockem.mixin.android import AndroidToolsMixin

from buildbot.buildslave import BuildSlave

class BaseSlave(IOSToolsMixin, AndroidToolsMixin):
    def __init__(self, name, password):
        self.name = name
        self.password = password
        self.max_builds = 2
        self.capabilities = Capabilities()
        self.properties = {}

    def setGit(self, version = None, installdir = None):
        self.setCapability('git', version, installdir)
        return self

    def setHg(self, version = None, installdir = None):
        self.setCapability('hg', version, installdir)
        return self

    def setUnity(self, version = None, installdir = None):
        self.setCapability('unity', version, installdir)
        return self
        
    def setUnity64(self, version = None, installdir = None):
        self.setCapability('unity64', version, installdir)
        return self

    def setNuGet(self, version = None, installdir = None):
        self.setCapability('nuget', version, installdir)
        return self

    def setXamarin(self, version = None, installdir = None):
        self.setCapability('xamarin', version, installdir)
        return self
        
    def setMaxBuilds(self, max_builds):
        self.max_builds = max_builds
        return self

    def createBuildslave(self):
        return BuildSlave(self.name, self.password,
            max_builds = self.max_builds,
            properties = dict(self.properties,
                capabilities = self.capabilities.to_dict(),
            ))

    def setCapability(self, name, version = None, installdir = None):
        self.capabilities.addTool(name, version, installdir)
        return self
        
    def isCapable(self, requirement):
        return not self.capabilities.getCompatibleToolVersion(requirement) is None

    def __str__(self):
        return self.name

