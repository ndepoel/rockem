# Rockem Buildbot #

### What is Rockem? ###

Rockem is a build configuration system built on top of [Buildbot](http://buildbot.net/), with the following goals:

* Declarative configuration of projects; describe _what_ you want to build, not _how_ it should be built.
* Quick configuration of build masters and slaves
* Automatic matching of project builds with compatible build slaves, based on requirements and capabilities.
* Quick and easy set up of common project types, including:
    * Visual Studio solutions
    * Xcode projects
    * Unity3D projects
    * Xamarin Android/iOS projects
    * Unreal Engine 4 projects and engine mods
* Automatic set up of common build variants, including:
    * Quick (run on every source change)
    * Full
    * Nightly (full build from clean source)
    * Release
* Collection and publishing of build artifacts

Rockem is very much a work in progress and built primarily for my own personal needs, so don't get angry if things change all of a sudden or if it doesn't do everything you want.

### How do I use it? ###

In short, you specify the properties of your projects and build slaves and from those specifications, Rockem will synthesize a Buildbot configuration.

For each project you specify:

* What is its name?
* What type of project is it? (Visual Studio, Xcode, Unity, Xamarin, etc)
* Where is it located? (In version control: Git, Mercurial, Subversion or Perforce)
* For every sub-project, where applicable:
    * What is the location of the project file?
    * What are the required tool versions?
    * Which build parameters are required?
    * How are the unit tests run?
    * What are the artifacts created by the build?

For each build slave you specify:

* What type of OS does it run on? (Windows, OS X, Linux)
* What are the credentials for it to register with the build master?
* Which tools does it have installed (and where)?

### Examples ###

* See the [examples](https://bitbucket.org/ndepoel/rockem/src/master/examples/) directory to get an impression of how projects and build slaves are configured.