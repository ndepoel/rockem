def setupChangeSources(config, projects):
    if not 'change_source' in config:
        config['change_source'] = []

    for project in projects:
        poller = project.getPoller()
        print('Adding {0} change source for project "{1}"'.format(
            project.getPollerName(), project))

        config['change_source'].append(poller)

