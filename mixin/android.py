from buildbot.steps import shell

class AndroidBuildMixin(object):
    def __init__(self):
        self.apk_filename = None
        self.device_id = None

    def setDeployApk(self, apk_filename, device_id = None):
        """
        Deploy an APK application package file to a device after building.
        Uses the ADB platform tool to deploy the package to a device.
        Adds the requirement 'androidsdk'.
        @param device_id    The identifier for the device to deploy to. If left empty,
                            the APK will be deployed to a single attached device.
        """
        self.setRequirement('androidsdk')
        self.apk_filename = apk_filename
        self.device_id = device_id
        
    def getDeploySteps(self, **kwargs):
        if not self.apk_filename:
            return []
        
        # TODO: retrieve Android SDK installdir from slave
        cmd = ['adb']
        if self.device_id:
            cmd.extend(['-s', self.device_id])
            
        cmd.extend(['install', '-rtd', self.apk_filename])
        return [shell.ShellCommand(
            name = 'deploy-apk',
            command = ' '.join(cmd),
            **dict(self.args, **kwargs)
        )]

class AndroidToolsMixin(object):
    def setAndroidSdk(self, version = None, installdir = None):
        return self.setCapability('androidsdk', version, installdir)

