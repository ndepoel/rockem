from buildbot.steps import shell

class IOSBuildMixin(object):
    def __init__(self):
        self.ipa_filename = None
        self.udid = None

    def setDeployIpa(self, ipa_filename, udid = None):
        """
        Deploy an IPA application archive file to a device after building.
        Uses the ideviceinstaller tool to deploy the archive to a device.
        Adds the requirement 'ideviceinstaller'.
        @param udid The unique device identifier for the device to deploy to.
                    If left empty, the IPA will be deployed to a single attached device.
        """
        self.setRequirement('ideviceinstaller')
        self.ipa_filename = ipa_filename
        self.udid = udid
        
    def getDeploySteps(self, **kwargs):
        if not self.ipa_filename:
            return []
    
        cmd = ['ideviceinstaller']
        if self.udid:
            cmd.extend(['-u', self.udid])
            
        cmd.extend(['-i', self.ipa_filename])
        return [shell.ShellCommand(
            name = 'deploy-ipa',
            command = ' '.join(cmd),
            **dict(self.args, **kwargs)
        )]

class IOSToolsMixin(object):
    def setIDeviceInstaller(self, version = None):
        return self.setCapability('ideviceinstaller', version)

