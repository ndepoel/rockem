#!/bin/bash

function cleanup {
    if [ -d "mnt/" ]; then
        sleep 1
        umount mnt/
        rmdir mnt/
    fi

    rm -rf instruments_data.trace screenshot-*.tiff *.log
}

function error {
    echo $1
    if [ -n "$SSHOT_PID" ]; then
        echo "Aborting screenshot process..."
        kill $SSHOT_PID
    fi
    cleanup
    exit 1
}

if [ "$#" -lt 3 ]; then
	error "Usage: $0 <benchmark name> <application> <duration> [device udid]"
fi

# Command-line arguments
NAME="$1"
APP_ID="$2"
DURATION="$3"
DEVICE_ID="${4:-$(idevice_id -l | sed -n 1,1p)}"	# Optional; default to first connected device if unspecified

if [ ${#DEVICE_ID} -ne 40 ]; then
    error "No device found or invalid device UDID!"
fi

if [ $(($DURATION)) -le 0 ]; then
    error "Invalid duration: $DURATION"
fi

echo "Running benchmark $NAME for application $APP_ID on device $DEVICE_ID for $DURATION seconds..."

# Run this in the background so a screenshot will be taken while instruments is still running 
(sleep $(($DURATION - 5)); idevicescreenshot -u $DEVICE_ID) &
SSHOT_PID=$!

# Run the app with Instruments profiling for the given duration
instruments \
    -w $DEVICE_ID \
    -t "/Applications/Xcode.app/Contents/Applications/Instruments.app/Contents/Resources/templates/Activity Monitor.tracetemplate" \
    -D instruments_data \
    -l $(($DURATION * 1000)) \
    $APP_ID \
    -e BENCHMARK $NAME \
    || error "Error while running Instruments!"

# Wait until the screenshot process is completed, because the instruments run might finish early in case of an app crash
wait $SSHOT_PID

echo 'Retrieving benchmark data...'
mkdir -p mnt
ifuse mnt -u $DEVICE_ID --documents $APP_ID || error "Error mounting iOS device storage!"
mv -fv mnt/*.log .

zip -r benchmark-$(date +%Y-%m-%d-%H-%M-%S)-$NAME.zip instruments_data.trace screenshot-*.tiff *.log
cleanup
