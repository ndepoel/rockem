#!/usr/bin/env python

import os, zipfile
import re, datetime, time
from collections import defaultdict

metrics = ['VirtualSize', 'ResidentSize']

def to_uint(data, size=4):
    return int(data[size-1::-1].encode('hex'), 16)

def find_string(stream, needle):
    if isinstance(needle, basestring):
        return find_string(stream, bytearray(needle))

    index = 0
    while True:
        data = stream.read(1)
        if len(data) == 0:
            return False

        b = ord(data[0])
        if b == needle[index]:
            index = index + 1
        else:
            index = 0

        if index == len(needle):
            return True

def process_run(run, app_name):
    result = []

    while True:
        if not find_string(run, app_name):
            break

        frame = {}
        for metric in metrics:
            if not find_string(run, metric):
                break

            data = run.read(8)
            if len(data) < 4 or ord(data[2]) != 0x84 or ord(data[3]) != 0x97:
                continue

            data = run.read(10)
            size = to_uint(data)
            frame[metric] = size

        if frame:
            result.append(frame)

    return result

def process_zipped_run(filename, app_name):
    with zipfile.ZipFile(filename) as run_zip:
        for run_name in run_zip.namelist():
            with run_zip.open(run_name) as run:
                return process_run(run, app_name)

def process_zipped_trace(filename, app_name):
    result = {}

    with zipfile.ZipFile(filename) as z:
        run_entries = filter(lambda x: x.endswith('.run.zip'), z.namelist())
        for run_entry in run_entries:
            run_name = os.path.basename(run_entry)
            run_zipfile = z.extract(run_entry, '/tmp')
            try:
                result[run_name] = process_zipped_run(run_zipfile, app_name)
            finally:
                os.unlink(run_zipfile)

    return result

def process_package(filename, app_name):
    # Extract metadata from the filename
    m = re.match(r'benchmark-((\d+-){6})(.+)\.zip', filename)
    if not m:
        return

    # Convert the date components into a datetime object
    date_parts = m.groups()[0].split('-')
    date_nums = map(lambda x: int(x), date_parts[0:6])
    dt = datetime.datetime(*date_nums)

    bench_name = m.groups()[2]

    result = process_zipped_trace(filename, app_name)
    return (bench_name, dt, result)

def convert_graphite(app_name, dt, data):
    lines = []

    for run_name in data:
        run = data[run_name]
        values = defaultdict(list)
        for frame in run:
            for metric in frame:
                value = frame[metric]
                values[metric].append(value)

        for metric in values:
            v = sorted(values[metric])
            derivatives = dict(
                peak = max(v),
                median = v[len(v) / 2])

            for derivative in derivatives:
                metric_path = '{}.{}.{}'.format(app_name, metric, derivative)
                value = derivatives[derivative]
                timestamp = int(time.mktime(dt.timetuple()))
                lines.append('{} {} {}'.format(metric_path, value, timestamp))

    return '\n'.join(lines)

if __name__ == '__main__':
    import sys

    app_name = sys.argv[1]
    for arg in sys.argv[2:]:
        bench_name, dt, data = process_package(arg, app_name)
        graphite_data = convert_graphite(app_name, dt, data)
        print(graphite_data)
