#!/bin/bash

BUILDBOT=$HOME/buildbot
MASTER=$BUILDBOT/master
ARTIFACTS=$HOME/public_html/artifacts
MAX_HISTORY=30  # days
ROCKEM_GIT=https://bitbucket.org/ndepoel/rockem.git

# Install basic requirements (master & slave)
sudo update-locale LC_ALL=${LC_ALL:-$LANG}
sudo apt-get install build-essential git mercurial subversion python python-dev python-pip python-virtualenv samba apache2

# Download Perforce command-line client for the correct processor architecture
MACHINE=$(uname -m)
if [[ $MACHINE == armv7* || $MACHINE == armv8* ]]; then
    P4ARCH=armhf
elif [[ $MACHINE == arm* ]]; then
    P4ARCH=armel
elif [[ $MACHINE == amd64 ]]; then
    P4ARCH=x86_64
elif [[ $MACHINE == i386 || $MACHINE == i686 ]]; then
    P4ARCH=x86
elif [[ $MACHINE == powerpc ]]; then
    P4ARCH=ppc
else
    # This includes x86, x86_64, ppc64 and ia64 architectures
    P4ARCH=$MACHINE
fi
wget http://www.perforce.com/downloads/perforce/r15.1/bin.linux26$P4ARCH/p4
sudo mv p4 /usr/bin/
sudo chown root.root /usr/bin/p4
sudo chmod +x /usr/bin/p4

# Install SSH keys for common source hosts (master & slave)
mkdir -p $HOME/.ssh
ssh-keyscan -t rsa github.com >> $HOME/.ssh/known_hosts
ssh-keyscan -t rsa bitbucket.org >> $HOME/.ssh/known_hosts

# Create and install buildbot environment (master & slave)
virtualenv --no-site-packages $BUILDBOT
source $BUILDBOT/bin/activate
pip install buildbot buildbot-slave

# Set up a directory for storing build artifacts
mkdir -p $ARTIFACTS
sudo a2enmod userdir
sudo service apache2 restart

# Set up a cronjob to automatically remove old artifacts
(crontab -l; echo "0 * * * * find $ARTIFACTS -mindepth 1 -mtime +$MAX_HISTORY -delete") | crontab -

# Convenience function to quickly create an empty Python package
function mkpkg {
    mkdir -p $1
    touch $1/__init__.py
}

# Create and set up build master
buildbot create-master $MASTER
git clone $ROCKEM_GIT $MASTER/rockem
mkpkg $MASTER/projects
mkpkg $MASTER/slaves
cp $MASTER/rockem/extras/master.cfg.rockem $MASTER/master.cfg

buildbot start $MASTER

