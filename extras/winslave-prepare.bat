@echo off
net session >nul 2>&1
if not %errorLevel% == 0 (
	echo Please run this script as Administrator.
	pause
	exit /b 1
)

set PS=powershell -NoProfile -ExecutionPolicy unrestricted

echo Installing Chocolatey...
%PS% -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
choco install -y nuget.commandline

echo Installing Python...
choco install -y python2

if exist C:\Python27\python.exe (
	set PYTHON=C:\Python27
) else if exist C:\tools\python2\python.exe (
	set PYTHON=C:\tools\python2
) else (
	echo Cannot find Python installation directory! Please check that it was installed correctly.
	pause
	exit /b 1
)

echo Installing version control clients...
choco install -y git hg sliksvn p4v

set /p INSTALL_VS=Do you want to install Visual Studio 2013 Community Edition? [y/N]
if /i "%INSTALL_VS%" == "y" (
    echo Installing Visual Studio 2013 Community Edition...
    choco install -y visualstudiocommunity2013
)

echo Installing pip packages...
set PIP=%PYTHON%\Scripts\pip.exe
%PIP% install virtualenv

echo Downloading PyWin32...
if defined ProgramFiles(x86) (
	set PYWIN32=http://netcologne.dl.sourceforge.net/project/pywin32/pywin32/Build 219/pywin32-219.win-amd64-py2.7.exe
) else (
	set PYWIN32=http://netcologne.dl.sourceforge.net/project/pywin32/pywin32/Build 219/pywin32-219.win32-py2.7.exe
)
%PS% -Command "(New-Object Net.WebClient).DownloadFile('%PYWIN32%', '%PYTHON%\pywin32.exe')"

pause
