#!/bin/bash

function check-tool {
    TOOL=$1
    NAME=$2

    which -s $TOOL
    if [ $? -ne 0 ]; then
        echo "Please install $NAME before running this script."
        exit 1
    fi
}

function build-project {
    PROJECT=$1
    BRANCH=$2

    REPO_URL=https://github.com/libimobiledevice/$PROJECT.git

    git clone -b $BRANCH $REPO_URL || git -C $PROJECT checkout $BRANCH
    pushd $PROJECT
    ./autogen.sh --prefix=/opt/local
    make || { echo "Building $PROJECT failed!"; exit 1; }
    sudo make install
    popd
}

check-tool xcode-select Xcode
check-tool port MacPorts

xcode-select --install
sudo port install pkgconfig automake autoconf libtool libusb libxml2 libgcrypt libzip osxfuse openssl curl

check-tool git Git

build-project libplist 1.12
build-project libusbmuxd 1.0.10
build-project libimobiledevice master
build-project libideviceactivation master
build-project libirecovery master

build-project ideviceinstaller master
build-project idevicerestore master
build-project ifuse master
