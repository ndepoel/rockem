from distutils.version import StrictVersion

from buildbot.interfaces import IRenderable

from zope.interface import implements

class ToolVersion(object):
    def __init__(self, tool, version, installdir):
        self.tool = tool
        self.version = StrictVersion(version) if version else None
        self.installdir = installdir
        
    @staticmethod
    def from_dict(d, tool):
        return ToolVersion(
            tool = tool,
            version = d.get('version'),
            installdir = d.get('installdir'),
        )
        
    def to_dict(self):
        return dict(
            version = str(self.version) if self.version else None,
            installdir = self.installdir,
        )
        
    def satisfies(self, requirement):
        try:
            if not requirement.satisfied_by(self.version):
                return False
        except:
            return False
        
        return True
        
    def getInstallDir(self):
        return self.installdir
        
    def __str__(self):
        if isinstance(self.version, basestring):
            return '{0} {1}'.format(self.tool.name, self.version)
        else:
            return self.tool.name

class Tool(object):
    def __init__(self, name):
        self.name = name
        self.versions = []
        
    @staticmethod
    def from_dict(d):
        tool = Tool(name = d.get('name'))
        tool.versions = [ToolVersion.from_dict(tvd, tool) for tvd in d.get('versions', [])]
        return tool
        
    def to_dict(self):
        return dict(
            name = self.name,
            versions = [v.to_dict() for v in self.versions],
        )
        
    def addVersion(self, version = None, installdir = None):
        toolversion = ToolVersion(self, version, installdir)
        self.versions.append(toolversion)
        return toolversion
        
    def getCompatibleVersion(self, requirement):
        if not self.versions:
            return None
        
        # Find a version that specifically satisfies the requirement
        # Prefer the latest version of a tool (reverse sort by version number)
        sorted_versions = sorted(self.versions, key = lambda v: v.version, reverse = True)
        for toolversion in sorted_versions:
            if toolversion.satisfies(requirement):
                return toolversion
                
        return None
        
    def __str__(self):
        return ', '.join(map(str, self.versions))
        
class Capabilities(object):
    def __init__(self):
        self.tools = {}
        
    @staticmethod
    def from_dict(d):
        caps = Capabilities()
        for item in d:
            tool = Tool.from_dict(item)
            caps.tools[tool.name] = tool
            
        return caps
        
    def to_dict(self):
        return [t.to_dict() for t in self.tools.values()]
        
    def addTool(self, name, version = None, installdir = None):
        if not name in self.tools:
            self.tools[name] = Tool(name)
        
        tool = self.tools[name]
        tool.addVersion(version, installdir)
        return tool
        
    def getCompatibleToolVersion(self, requirement):
        if not requirement.name in self.tools:
            return None
        
        tool = self.tools[requirement.name]
        return tool.getCompatibleVersion(requirement)
        
    def getToolInstalldir(self, requirement):
        toolversion = self.getCompatibleToolVersion(requirement)
        if not toolversion:
            return None
            
        return toolversion.getInstallDir()

    def __str__(self):
        return "\n".join(map(str, self.tools.values()))
    
    @staticmethod    
    def getInstallDir(props, requirement, default = None):
        capabilities = Capabilities.from_dict(props.getProperty('capabilities'))
        return capabilities.getToolInstalldir(requirement) or default

class ToolInstallDir(object):
    implements(IRenderable)
    
    def __init__(self, requirement, **defaults):
        self.requirement = requirement
        self.defaults = defaults
        
    def getRenderingFor(self, props):
        os = props.getProperty('os')
        default = self.defaults.get(os)
        return Capabilities.getInstallDir(props, self.requirement, default)

