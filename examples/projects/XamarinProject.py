from rockem.project.xamarin import XamarinProject

p = ProjectConfig = XamarinProject('Rockem Xamarin Test') \
    .setGit('git@bitbucket.org:ndepoel/rockem-xamarin-test.git') \
    .setBranch('master')

p.androidBuild() \
    .setProjectFile('Droid/RockemXamarinTest.Droid.csproj') \
    .setArtifacts('Droid/bin/Release/*.apk')

p.iosBuild() \
    .setProjectFile('iOS/RockemXamarinTest.iOS.csproj') \
    .setArtifacts('iOS/bin/iPhone/Release/*.ipa')

