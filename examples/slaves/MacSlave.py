from rockem.slave.macosx import MacSlave

s = SlaveConfig = MacSlave('MacBook Pro', 'password') \
    .setXcode('6.3') \
    .setGit('2.3.2') \
    .setHg('3.3') \
    .setXamarin('5.9') \
    .setUnity('5.0.1')

