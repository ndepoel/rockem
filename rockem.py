from slaves import setupSlaves
from change_source import setupChangeSources
from builders import setupBuilders
from schedulers import setupSchedulers

import pkgutil, imp

def loadModules(path):
    modules = []
    module_paths = [path]
    module_names = [name for _, name, is_pkg in pkgutil.iter_modules(module_paths) if not is_pkg]

    for module_name in module_names:
        print('Loading module: {0}.{1}'.format(path, module_name))

        module_info = imp.find_module(module_name, module_paths)
        module = imp.load_module(module_name, *module_info)
        modules.append(module)

    return modules
    
def loadConfigs(path, config_name):
    return [getattr(module, config_name) for module in loadModules(path) if hasattr(module, config_name)]

def setup(config):
    projects = loadConfigs('projects', 'ProjectConfig')
    slaves = loadConfigs('slaves', 'SlaveConfig')

    # Add all build slaves
    setupSlaves(config, slaves)

    # Add change sources per project
    setupChangeSources(config, projects)
    
    # Create builder configs for build/slave combinations
    setupBuilders(config, projects, slaves)

    # Create various schedulers for all projects and project builds
    setupSchedulers(config, projects)

