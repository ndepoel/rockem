from collections import OrderedDict

class BuildPhase(object):
    NONE = set()

    CLEAN = set(['clean'])
    BUILD = set(['build'])
    TEST = set(['test'])
    BIGTEST = set(['bigtest'])
    ANALYZE = set(['analyze'])
    EXPORT = set(['export'])
    PUBLISH = set(['publish'])
    DEPLOY = set(['deploy'])
    BENCHMARK = set(['benchmark'])
    BETA = set(['beta'])
    RELEASE = set(['release'])
    
    FULLTEST = TEST | BIGTEST | ANALYZE

class BuildVariant(object):
    NONE = BuildPhase.NONE

    QUICK = BuildPhase.BUILD | BuildPhase.TEST | BuildPhase.PUBLISH
    CLEAN = BuildPhase.CLEAN | BuildPhase.BUILD | BuildPhase.PUBLISH
    FULL = BuildPhase.BUILD | BuildPhase.FULLTEST | BuildPhase.EXPORT | BuildPhase.PUBLISH
    NIGHTLY = CLEAN | FULL
    BENCHMARK = FULL | BuildPhase.DEPLOY | BuildPhase.BENCHMARK
    BETA = FULL | BuildPhase.BETA
    RELEASE = FULL | BuildPhase.RELEASE

    ALL = [QUICK, CLEAN, FULL, NIGHTLY, BENCHMARK, BETA, RELEASE]

class BuildCategory(object):
    def __init__(self, name, label, *variants):
        self.name = name
        self.label = label
        self.variants = OrderedDict(variants)
        
    def getVariantNames(self):
        return self.variants.keys()

BuildCategory.REGULAR = BuildCategory('regular', '',
    ('Quick', BuildVariant.QUICK),
    ('Clean', BuildVariant.CLEAN),
    ('Full', BuildVariant.FULL),
    ('Nightly', BuildVariant.NIGHTLY),
    ('Benchmark', BuildVariant.BENCHMARK))

BuildCategory.RELEASE = BuildCategory('release', ' Release',
    ('Beta', BuildVariant.BETA),
    ('Release', BuildVariant.RELEASE))

BuildCategory.ALL = [BuildCategory.REGULAR, BuildCategory.RELEASE]

BuildVariant.INDEX = {}
for category in BuildCategory.ALL:
    BuildVariant.INDEX.update(category.variants)

