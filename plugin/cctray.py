from buildbot.interfaces import IStatusReceiver
from buildbot.status.results import SUCCESS, FAILURE, EXCEPTION

from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from twisted.application import service, strports
from twisted.internet.defer import inlineCallbacks

from zope.interface import implements

from xml.dom.minidom import Document

from datetime import datetime

class CCTrayStatus(service.MultiService):
    implements(IStatusReceiver)

    def __init__(self, http_port=8020, resource='cc.xml'):
        service.MultiService.__init__(self)
        if isinstance(http_port, int):
            http_port = "tcp:%d" % http_port
            
        self.http_port = http_port
        self.resource = resource
        self.service = None

    def setServiceParent(self, parent):
        self.master = parent.master
        
        if not self.service:
            root = Resource()
            root.putChild(self.resource, CCXml(self.master))
            self.service = strports.service(self.http_port, Site(root))
            self.service.setServiceParent(self)
        
        service.MultiService.setServiceParent(self, parent)
        
    def checkConfig(self, otherStatusReceivers):
        pass

BuildStatus = {
    SUCCESS: 'Success',
    FAILURE: 'Failure',
    EXCEPTION: 'Exception',
}

class CCXml(Resource):
    isLeaf = True

    def __init__(self, master):
        Resource.__init__(self)
        self.master = master

    def _formatTime(self, timestamp):
        return datetime.fromtimestamp(timestamp).replace(microsecond=0).isoformat()

    @inlineCallbacks
    def _renderXml(self, request):
        request.setHeader('Content-Type', 'application/xml')
        
        doc = Document()
        projects = doc.createElement('Projects')
        
        status = self.master.getStatus()
        schedulers = status.getSchedulers()
        
        for builderName in status.getBuilderNames():
            builder = status.getBuilder(builderName)
            lastBuild = builder.getLastFinishedBuild()
            pending = yield builder.getPendingBuildRequestStatuses()
            
            upcoming = []
            for s in schedulers:
                if builderName in s.listBuilderNames():
                    upcoming.extend(s.getPendingBuildTimes())
            
            project = doc.createElement('Project')
            project.setAttribute('name', builderName)
            project.setAttribute('webUrl', status.getURLForThing(builder))
            project.setAttribute('activity', 'Building' if builder.getCurrentBuilds() else 'Sleeping')
            
            if lastBuild:
                startTime, endTime = lastBuild.getTimes()
                project.setAttribute('lastBuildLabel', str(lastBuild.getNumber()))
                project.setAttribute('lastBuildTime', self._formatTime(startTime))
                project.setAttribute('lastBuildStatus', BuildStatus.get(lastBuild.getResults(), 'Unknown'))
            
            if upcoming:
                project.setAttribute('nextBuildTime', self._formatTime(min(upcoming)))
            
            projects.appendChild(project)
        
        doc.appendChild(projects)
        
        request.write(doc.toprettyxml())
        request.finish()

    def render_GET(self, request):
        self._renderXml(request)
        return NOT_DONE_YET

