from category import BuildPhase, BuildVariant, BuildCategory
import util

from buildbot.process.factory import BuildFactory
from buildbot.config import BuilderConfig

def getSteps(steps_func, phase, haltOnFailure = True):
    return steps_func(
        doStepIf = util.hasPhase(phase),
        hideStepIf = util.skipped,
        haltOnFailure = haltOnFailure)

def gatherBuildSteps(build):
    steps = []
    
    # Checkout
    steps.extend(build.project.getSourceSteps())
    
    # Build
    steps.extend(getSteps(build.getCleanSteps, BuildPhase.CLEAN))
    steps.extend(getSteps(build.getBuildSteps, BuildPhase.BUILD))
    steps.extend(getSteps(build.getTestSteps, BuildPhase.TEST))
    steps.extend(getSteps(build.getBigTestSteps, BuildPhase.BIGTEST))
    steps.extend(getSteps(build.getAnalyzeSteps, BuildPhase.ANALYZE))
    steps.extend(getSteps(build.getExportSteps, BuildPhase.EXPORT))
    
    # Publish
    # TODO: add publish steps, split into phases: publish, release
    steps.extend(getSteps(build.getPublishSteps, BuildPhase.PUBLISH))
    steps.extend(getSteps(build.getDeploySteps, BuildPhase.DEPLOY))
    
    return steps

def areCompatible(build, slave):
    requirements = build.getRequirements()

    # Check the slave's capabilities for all requirements 
    for requirement in requirements:
        if not slave.isCapable(requirement):
            return False
        
    return True

def createBuildBuilder(build, slaves):
    result = []

    compat_slaves = [slave.name for slave in slaves if areCompatible(build, slave)]
    if not compat_slaves:
        print('[WARNING] No compatible build slaves for build "{0}"'.format(build))
        return None

    print('Creating builder "{0}" with build slaves: {1}'.format(build, compat_slaves))

    factory = BuildFactory(gatherBuildSteps(build))
    factory.useProgress = False

    return BuilderConfig(
        name = str(build),
        slavenames = compat_slaves,
        factory = factory,
        properties = {
            'excluded_variants': build.excluded_variants,
        },
    )

def createProjectBuilders(project, slaves):
    result = []
    for build in project.builds:
        builder = createBuildBuilder(build, slaves)
        if builder:
            result.append(builder)
            
    return result

def setupBuilders(config, projects, slaves):
    if not 'builders' in config:
        config['builders'] = []

    for project in projects:
        project.builders = createProjectBuilders(project, slaves)
        config['builders'].extend(project.builders)

