from category import BuildVariant

from buildbot.process import properties
from buildbot.status.results import SKIPPED

def getStepPhases(step):
    variant = step.getProperty('variant')
    excluded = step.getProperty('excluded_variants')
    if excluded and variant in excluded:
        return BuildVariant.NONE
    
    return BuildVariant.INDEX[variant]

def hasPhase(phase):
    return lambda step: getStepPhases(step) & phase

def notHasPhase(phase):
    return lambda step: not (getStepPhases(step) & phase)

def ifPhase(phase, trueResult, falseResult):
    return lambda props: trueResult if getStepPhases(props) & phase else falseResult

def skipped(results, step):
    return results == SKIPPED

