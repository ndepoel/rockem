from rockem.category import BuildCategory, BuildVariant, BuildPhase
from rockem.steps import transfer
from rockem import config, util

from buildbot.changes import gitpoller, hgpoller
from buildbot.steps.source import git, mercurial
from buildbot.steps import master
from buildbot.process import properties

from distutils.versionpredicate import VersionPredicate

import re

class Project(object):
    def __init__(self, name):
        self.name = name
        self.requirements = {}
        self.pollinterval = 60
        self.builds = []
        self.publishers = []
        self.branch = None
        self.branches = None
        self.branch_arg = None

    def setUsername(self, username):
        self.username = username
        return self

    def setPassword(self, password):
        self.password = password
        return self

    def setGit(self, repourl):
        """
        Sets the project's repository path to a Git repository.
        Automatically adds a Git change poller and checkout step,
        and Git as a requirement for the build slave.
        The default branch for a Git repository is 'master'.
        """
        self.repourl = repourl

        self.poller_class = gitpoller.GitPoller
        self.checkout_class = git.Git
        self.checkout_kwargs = {'submodules': True}
        self.branch_arg = 'branch'
        self.default_branch = 'master'
        self.setRequirement('git', '>= 1.7')
        return self

    def setHg(self, repourl):
        """
        Sets the project's repository path to a Mercurial repository.
        Automatically adds a Mercurial change poller and checkout step,
        and Mercurial as a requirement for the build slave.
        The default branch for a Mercurial repository is 'default'.
        """
        self.repourl = repourl

        self.poller_class = hgpoller.HgPoller
        self.checkout_class = mercurial.Mercurial
        self.checkout_kwargs = {'branchType': 'inrepo'}
        self.branch_arg = 'defaultBranch'
        self.default_branch = 'default'
        self.setRequirement('hg', '>= 2.0')
        return self

    def setBranch(self, branch):
        """
        Sets the version control branch to poll for changes and to check out by
        default when none is given on a forced build.
        """
        self.branch = branch
        return self

    def setBranches(self, branches):
        """
        Sets the version control branches to poll for changes and to check out by
        default when none is given on a forced build.
        Can be a list of branch names, a boolean value True for all branches, or
        a callable that takes a branch names and returns whether that branch should
        be polled.
        """
        self.branches = branches
        return self
        
    def setBranchRegex(self, pattern):
        if isinstance(pattern, basestring):
            pattern = [pattern]
            
        regexes = [re.compile(p) for p in pattern]
        
        def matchBranch(branch):
            for regex in regexes:
                if regex.search(branch):
                    return True
                    
            return False
            
        return self.setBranches(matchBranch)

    def setPollInterval(self, pollinterval):
        """
        Sets the polling interval in seconds for the version control change poller.
        Defaults to 60 seconds.
        """
        self.pollinterval = pollinterval
        return self
        
    def createBuild(self, build_class, name = None):
        """
        Creates and adds a Build of the specified class to this Project.
        Most project types will include shorthand methods for easy creation of
        common builds, but this method allows you to create uncommon combinations
        of builds within a project. For example, if you have various different
        project types within the same source repository.
        """
        build = build_class(self, name)
        self.builds.append(build)
        return build
        
    def setRequirement(self, name, version_str = None):
        """
        Adds a requirement that any build slave has to meet in order to support this Project.
        You can optionally include version requirements in the form of a predicate, e.g.:
        - '>= 4.2'
        - '== 2.0.3'
        - '>= 5.0, < 6.0'
        """
        if version_str:
            predicate_str = '{0} ({1})'.format(name, version_str)
        else:
            predicate_str = name
            
        self.requirements[name] = VersionPredicate(predicate_str)
        return self
        
    def getBranch(self):
        return self.branch or self.default_branch

    def getPollerName(self):
        return self.poller_class.__name__

    def getPoller(self):
        """
        Creates a source change poller based on the version control repository set for this Project.
        @see setGit
        @see setHg
        """
        if self.branches:
            branch_args = {'branches': self.branches}
        else:
            branch_args = {'branch': self.getBranch()}
            
        return self.poller_class(
            repourl = self.repourl,
            project = self.name,
            workdir = '{0}-{1}-workdir'.format(self.name, self.getPollerName()),
            pollinterval = self.pollinterval,
            **branch_args)

    def getSourceSteps(self):
        """
        Creates source checkout build steps based on the version control repository set for this Project.
        @see setGit
        @see setHg
        """
        # Set the default branch to check out, if none is supplied by the change poller or forced build.
        if self.branch_arg:
            self.checkout_kwargs[self.branch_arg] = self.getBranch()
    
        # We create two separately configured source steps here and choose between them with doStepIf, 
        # because not all version control steps allow 'mode' and 'method' to be renderable.
        return [
            self.checkout_class(
                name = 'source-quick',
                repourl = self.repourl,
                mode = 'incremental',
                doStepIf = util.notHasPhase(BuildPhase.CLEAN),
                hideStepIf = util.skipped,
                **self.checkout_kwargs),
            self.checkout_class(
                name = 'source-fresh',
                repourl = self.repourl,
                mode = 'full',
                method = 'fresh',
                doStepIf = util.hasPhase(BuildPhase.CLEAN),
                hideStepIf = util.skipped,
                **self.checkout_kwargs),
        ]

    def __str__(self):
        return self.name

class BaseBuild(object):
    def __init__(self, project, name, suffix):
        self.project = project
        if name and suffix:
            self.name = '{0} {1}'.format(name, suffix)
        elif name:
            self.name = name
        elif suffix:
            self.name = suffix
        else:
            raise ValueError('Neither name nor suffix supplied')
            
        self.excluded_variants = []
        self.requirements = {}
        self.artifacts = {}
        self.args = {}

    def setExcludedVariants(self, *variants):
        self.excluded_variants.extend(variants)
        return self

    def setArtifacts(self, filter, destination = None):
        self.artifacts[filter] = destination
        return self
        
    def setRequirement(self, name, version_str = None):
        """
        Adds a requirement that any build slave has to meet in order to support this Build.
        @see Project.setRequirement
        """
        if version_str:
            predicate_str = '{0} ({1})'.format(name, version_str)
        else:
            predicate_str = name
            
        self.requirements[name] = VersionPredicate(predicate_str)
        return self
        
    def setArgs(self, **args):
        """
        Set additional arguments to be applied on the created build steps.
        For advanced use only.
        """
        self.args = args
        return self

    def getRequirements(self):
        """
        Returns a list of requirements for running this build. This includes
        requirements inherited from the owning Project object, as well as
        requirements specific for this Build.
        """
        reqs = self.requirements.values()
        reqs.extend(self.project.requirements.values())
        return reqs
      
    def getCleanSteps(self, **kwargs):
        """
        Returns a list of steps to run the Clean phase of this build.
        Only required if the build needs to clean up outside its build directory.
        Normally this is already taken care of by a fresh source checkout.
        """
        return []

    def getBuildSteps(self, **kwargs):
        """
        Returns a list of steps to run the Build phase of this build.
        This usually includes a source compilation step.
        Must be implemented by subclasses.
        """
        raise NotImplementedError()
        
    def getTestSteps(self, **kwargs):
        """
        Returns a list of steps to run the Test phase of this build.
        This should include only lightweight (unit) tests that can and should
        be run on every source change.
        """
        return []
        
    def getBigTestSteps(self, **kwargs):
        """
        Returns a list of steps to run the Big Test phase of this build.
        This includes the expensive tests that should only be run during
        Nightly and Release build variants.
        """
        return []
        
    def getAnalyzeSteps(self, **kwargs):
        """
        Returns a list of steps to run the Analyze phase of this build.
        This includes steps for static code analysis, such as complexity
        measurements and vulnerability detection.
        """
        return []
        
    def getExportSteps(self, **kwargs):
        """
        Returns a list of steps to run the Export phase of this build.
        This usually includes some sort of packaging step.
        """
        return []

    def getDeploySteps(self, **kwargs):
        """
        Returns a list of steps to run the Deploy phase of this build.
        """
        return []
        
    def getPublishSteps(self, **kwargs):
        """
        Returns a list of steps to run the Publish phase of this build.
        By default, this copies the build artifacts from the build slave
        to a central location on the build master.
        This can be extended with steps that publish the artifacts to
        other destinations.
        """
        if not self.artifacts:
            return []
        
        steps = [self._createFileUploadStep(item) for item in self.artifacts.items()]
        
        # Files are copied to the master with highly restricted access rights by default,
        # so we need to change the file and directory modes if we want to expose them.
        steps.append(master.MasterShellCommand(
            name='set-permissions',
            command=properties.Interpolate('chmod -R a+rX "%(prop:buildername)s"'),
            path=config.artifacts_path,
        ))
        
        return steps
        
    def _createFileUploadStep(self, artifact_pair):
        glob_filter, destination = artifact_pair
        
        relative_dest = '%(prop:buildername)s/Build %(prop:buildnumber)s/{0}'.format(destination or '')
        
        masterdest = '{0}/{1}'.format(config.artifacts_path, relative_dest)
        url = '{0}/{1}'.format(config.artifacts_baseurl, relative_dest)
        
        return transfer.GlobFileUpload(
            name='collect-artifacts',
            glob_path='build/' + glob_filter,
            masterdest=properties.Interpolate(masterdest),
            url=properties.Interpolate(url),
            url_name='artifacts',
            workdir='',
            compress='gz',
            mode=0644,
        )

    def _combineSteps(self, *args, **kwargs):
        result = []
        for stepsFunc in args:
            steps = stepsFunc(self, **kwargs)
            result.extend(steps)
            
        return result

    def __str__(self):
        return '{0} {1}'.format(self.project, self.name)

