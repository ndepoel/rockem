from rockem.project.projectfile import ProjectFileProject, ProjectFileBuild
from rockem.category import BuildPhase
from rockem.steps import xamarin
from rockem.tool import ToolInstallDir
from rockem import util

from rockem.mixin.ios import IOSBuildMixin
from rockem.mixin.android import AndroidBuildMixin

from buildbot.steps import shell

class XamarinProject(ProjectFileProject):
    def __init__(self, name):
        ProjectFileProject.__init__(self, name)
        
    def androidBuild(self, name = None):
        """
        Creates and adds a XamarinAndroidBuild to this Project.
        """
        return self.createBuild(XamarinAndroidBuild, name) \
            .setSolutionFile(self.solution)
        
    def iosBuild(self, name = None):
        """
        Creates and adds a XamarinIOSBuild to this Project.
        """
        return self.createBuild(XamarinIOSBuild, name) \
            .setSolutionFile(self.solution)
        
class BaseXamarinBuild(ProjectFileBuild):
    def __init__(self, project, name, suffix):
        ProjectFileBuild.__init__(self, project, name, suffix)
        self.config = 'Release'
        self.setRequirement('nuget')
        self.setRequirement('xamarin')
        
    def getBuildSteps(self, **kwargs):
        return [
            self._nugetStep(),
            self._xbuildStep('build', 'Build', **kwargs),
        ]
        
    def _nugetStep(self):
        return shell.ShellCommand(
            name = 'nuget-restore',
            command = ['nuget', 'restore', self.solution if self.solution else ''],
        )
        
    def _xbuildStep(self, name, target, properties = None, **kwargs):
        requirement = self.requirements['xamarin']
    
        return xamarin.XBuild(
            name = 'xamarin-{0}'.format(name),
            installdir = ToolInstallDir(requirement),
            target = target,
            config = self.config,
            properties = properties,
            projectfile = self.projectfile,
            **dict(self.args, **kwargs)
        )
        
class XamarinAndroidBuild(BaseXamarinBuild, AndroidBuildMixin):
    def __init__(self, project, name):
        BaseXamarinBuild.__init__(self, project, name, 'Xamarin.Android')
        AndroidBuildMixin.__init__(self)
        
    def getExportSteps(self, **kwargs):
        return [self._xbuildStep('export', 'SignAndroidPackage', **kwargs)]
        
    def getDeploySteps(self, **kwargs):
        return AndroidBuildMixin.getDeploySteps(self, **kwargs)
    
class XamarinIOSBuild(BaseXamarinBuild, IOSBuildMixin):
    def __init__(self, project, name):
        BaseXamarinBuild.__init__(self, project, name, 'Xamarin.iOS')
        IOSBuildMixin.__init__(self)
        self.setRequirement('xcode')

    def getExportSteps(self, **kwargs):
        properties = {
            'Platform': 'iPhone',
            'BuildIpa': 'true',
            'CodesignKey': util.ifPhase(BuildPhase.RELEASE, 'iPhone Distribution', 'iPhone Developer'),
        }
        return [self._xbuildStep('export', 'Build', properties, **kwargs)]

    def getDeploySteps(self, **kwargs):
        return IOSBuildMixin.getDeploySteps(self, **kwargs)

