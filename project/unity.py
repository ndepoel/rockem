from rockem.project.project import Project, BaseBuild
from rockem.tool import ToolInstallDir
from rockem.steps import unity

from rockem.mixin.ios import IOSBuildMixin
from rockem.mixin.android import AndroidBuildMixin

class UnityProject(Project):
    def __init__(self, name):
        Project.__init__(self, name)

    def unityBuild(self, name = None):
        return self.createBuild(UnityBuild, name)
        
    def unity64Build(self, name = None):
        return self.createBuild(Unity64Build, name)

class UnityBuild(BaseBuild, IOSBuildMixin, AndroidBuildMixin):
    app_name = 'unity'
    app_suffix = 'Unity'

    unit_tests = None
    test_result_file = None
    build_target = None
    build_player = None
    custom_build = None

    def __init__(self, project, name):
        BaseBuild.__init__(self, project, name, self.app_suffix)
        IOSBuildMixin.__init__(self)
        AndroidBuildMixin.__init__(self)
        
        self.setRequirement(self.app_name)

    def setVersion(self, version):
        self.setRequirement(self.app_name, version)
        return self
        
    def setBuildTarget(self, build_target):
        self.build_target = build_target
        return self
        
    def setBuildPlayer(self, build_player):
        if not build_player in unity.Unity.PlayerTypes:
            raise ValueError('Unsupported Unity Player type: {0}'.format(build_player))
    
        self.build_player = build_player
        self.build_target = build_player
        return self.setArtifacts('Build/{0}*'.format(unity.Unity.PlayerTypes[build_player]))
        
    def setCustomBuild(self, build_method):
        self.custom_build = build_method
        return self

    def setUnitTests(self, test_method = 'UnityTest.Batch.RunUnitTests', result_file = 'UnitTestResults.xml'):
        self.unit_tests = test_method
        self.test_result_file = result_file
        return self

    def getBuildSteps(self, **kwargs):
        return self._getUnitySteps('load', 'Loading', **kwargs)
        
    def getTestSteps(self, **kwargs):
        if not self.unit_tests:
            return []
    
        return self._getUnitySteps('test', 'Testing', execute_method = self.unit_tests,
            extra_logfiles = {'UnitTestResults': self.test_result_file} if self.test_result_file else None,
            **kwargs)
        
    def getExportSteps(self, **kwargs):
        if not (self.build_player or self.custom_build):
            return []
    
        return self._getUnitySteps('export', 'Exporting', 
            build_player = self.build_player, execute_method = self.custom_build,
            **kwargs)
        
    def getDeploySteps(self, **kwargs):
        return self._combineSteps(
            IOSBuildMixin.getDeploySteps,
            AndroidBuildMixin.getDeploySteps,
            **kwargs)
        
    def _getUnitySteps(self, name, description, build_player = None, execute_method = None, extra_logfiles = None, **kwargs): 
        requirement = self.requirements[self.app_name]
    
        return [unity.Unity(
            name = '{0}-{1}'.format(self.app_name, name),
            installdir = ToolInstallDir(requirement),
            buildTarget = self.build_target,
            buildPlayer = build_player,
            executeMethod = execute_method,
            description = '{0} {1} project'.format(description, self.app_suffix),
            extraLogfiles = extra_logfiles,
            **dict(self.args, **kwargs)
        )]

class Unity64Build(UnityBuild):
    app_name = 'unity64'
    app_suffix = 'Unity 64-bit'

