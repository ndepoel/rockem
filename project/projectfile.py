from rockem.project.project import Project, BaseBuild
from rockem.category import BuildCategory, BuildVariant

from buildbot.steps import shell, vstudio

class ProjectFileProject(Project):
    def __init__(self, name):
        Project.__init__(self, name)
        self.solution = None

    def setSolutionFile(self, solutionfile):
        """
        Sets the relative path of the solution file for this Project.
        This solution file is shared amongst all builds created for this project,
        unless overridden in a build.
        """
        self.solution = solutionfile
        return self

    def visualStudioBuild(self, vs_edition, name = None):
        """
        Creates and adds a VisualStudioBuild to this Project.
        A specific edition of Visual Studio needs to be supplied.
        @see VisualStudioBuild.setEdition
        """
        return self.createBuild(VisualStudioBuild, name) \
            .setSolutionFile(self.solution) \
            .setEdition(vs_edition)

    def xcodeBuild(self, name = None):
        """
        Creates and adds an XcodeBuild to this Project.
        """
        return self.createBuild(XcodeBuild, name) \
            .setSolutionFile(self.solution)

class ProjectFileBuild(BaseBuild):
    def __init__(self, project, name, suffix):
        BaseBuild.__init__(self, project, name, suffix)
        self.solution = None
        self.projectfile = None
        self.projectname = None
        self.config = None

    def setSolutionFile(self, solutionfile):
        """
        Sets the relative path of the solution file to build.
        This overrides the setting inherited from the owning Project object.
        """
        self.solution = solutionfile
        return self

    def setProjectFile(self, projectfile):
        """
        Sets the relative path of the project file to build.
        Depending on the project type this can also be a solution file to build,
        for example if you have a project with multiple solution files and want
        to have a build for each individual solution.
        """
        self.projectfile = projectfile
        return self

    def setProjectName(self, projectname):
        """
        Sets the name of a specific project or target to build.
        The exact semantics of this name depend on the type of build.
        """
        self.projectname = projectname
        return self

    def setConfiguration(self, config):
        self.config = config
        return self

class VisualStudioBuild(ProjectFileBuild):
    supported_editions = {
        'vs2003': vstudio.VS2003,
        'vs2005': vstudio.VS2005,
        'vs2008': vstudio.VS2008,
        'vs2010': vstudio.VS2010,
        'vs2012': vstudio.VS2012,
        'vs2013': vstudio.VS2013
    }

    def __init__(self, project, name):
        ProjectFileBuild.__init__(self, project, name, 'MSVS')
        self.vs_edition = None
        self.config = 'Release'

    def setEdition(self, vs_edition):
        """
        Sets the edition of Visual Studio to use for this Build.
        This doubles as a requirement for the build slaves. Supported values are:
        - vs2003
        - vs2005
        - vs2008
        - vs2010
        - vs2012
        - vs2013
        """
        self.vs_edition = vs_edition
        self.buildstep_class = VisualStudioBuild.supported_editions.get(self.vs_edition)
        if self.buildstep_class is None:
            raise NotImplementedError('Unsupported Visual Studio edition: {0}'.format(self.vs_edition))

        self.setRequirement(self.vs_edition)
        return self

    def getBuildSteps(self, **kwargs):
        installdir = self.buildstep_class.default_installdir \
            .replace('Program Files', 'Program Files (x86)')

        return [self.buildstep_class(
            name = '{0}-build'.format(self.vs_edition),
            installdir = installdir,
            mode = 'build',
            projectfile = self.projectfile,
            project = self.projectname,
            config = self.config,
            **dict(self.args, **kwargs)
        )]

class XcodeBuild(ProjectFileBuild):
    def __init__(self, project, name):
        ProjectFileBuild.__init__(self, project, name, 'Xcode')
        self.setRequirement('xcode')

    def setVersion(self, version):
        self.setRequirement('xcode', version)
        return self

    def getCleanSteps(self, **kwargs):
        return self._xcodeSteps('clean', ['clean'], **kwargs)

    def getBuildSteps(self, **kwargs):
        return self._xcodeSteps('build', ['build'], **kwargs)
        
    def _xcodeSteps(self, name, actions, **kwargs):
        cmd = ['xcodebuild', '-project', self.projectfile]
        
        if self.projectname:
            cmd.extend(['-target', self.projectname])
        else:
            cmd.extend(['-alltargets'])
            
        # TODO: see if we can use self.config to select Debug/Release
    
        cmd.extend(actions)
    
        return [shell.ShellCommand(
            name = 'xcode-{0}'.format(name),
            command = cmd,
            **dict(self.args, **kwargs)
        )]

