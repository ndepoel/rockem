from rockem.project.project import Project, BaseBuild

from buildbot.steps import shell

class ScriptProject(Project):
    def __init__(self, name):
        Project.__init__(self, name)
        
    def scriptBuild(self, name = None):
        return self.createBuild(ScriptBuild, name)
        
class ScriptBuild(BaseBuild):
    def __init__(self, project, name):
        BaseBuild.__init__(self, project, name, 'Script')
        self.command = None
        self.workdir = None
        self.logfiles = []
        
    def setCommand(self, command):
        self.command = command
        return self
        
    def setWorkdir(self, workdir):
        self.workdir = 'build/{0}'.format(workdir)
        return self
        
    def setLogfiles(self, logfiles):
        self.logfiles = logfiles
        return self
        
    def getBuildSteps(self, **kwargs):
        return [shell.ShellCommand(
            name = 'script-execute',
            command = self.command,
            workdir = self.workdir,
            description = 'Executing script',
            logfiles = self.logfiles,
            **dict(self.args, **kwargs)
        )]
