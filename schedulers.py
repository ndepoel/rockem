from . import config
from category import BuildCategory, BuildVariant

from buildbot.schedulers.basic import SingleBranchScheduler
from buildbot.schedulers.timed import Nightly
from buildbot.schedulers.forcesched import ForceScheduler, FixedParameter, IntParameter, StringParameter, ChoiceStringParameter
from buildbot.changes.filter import ChangeFilter

# Create single branch schedulers, using quick build variant
def createChangeSchedulers(projects):
    result = []
    for project in projects:
        scheduler = SingleBranchScheduler(
            name = '{0} Change'.format(project),
            change_filter = ChangeFilter(
                branch = project.getBranch(),
                project = project.name),
            treeStableTimer = 10,
            properties = {'variant': 'Quick'},
            builderNames = [b.name for b in project.builders])
            
        result.append(scheduler)
        
    return result

# Create nightly schedulers for each project, starting at 3:00 and with 15-minute intervals    
def createNightlySchedulers(projects):
    hour = config.nightly_starthour
    minute = 0

    result = []
    for project in projects:
        scheduler = Nightly(
            name = '{0} Nightly'.format(project),
            branch = project.getBranch(),
            properties = {'variant': 'Nightly'},
            builderNames = [b.name for b in project.builders],
            hour = hour,
            minute = minute)
            
        result.append(scheduler)
        hour_delta, minute = divmod(minute + config.nightly_interval, 60)
        hour += hour_delta
            
    return result

def createForceScheduler(builders, name, category, *additional_props):
    return ForceScheduler(
        name = name,
        properties = [
            ChoiceStringParameter(
                'variant', 'Build variant:',
                choices=category.getVariantNames(),
                strict=True)
        ] + list(additional_props),
        builderNames=[b.name for b in builders]
    )

# All regular build variants can be force built
def createRegularScheduler(builders):
    return createForceScheduler(builders, 'Force', BuildCategory.REGULAR)

def createReleaseScheduler(builders):
    return createForceScheduler(builders, 'Release', BuildCategory.RELEASE,
        StringParameter('version_name', 'Version name:', required = True),
        IntParameter('version_number', 'Version number:'),
    )
        
def setupSchedulers(config, projects):
    if not 'schedulers' in config:
        config['schedulers'] = []

    s = config['schedulers']
    s.extend(createChangeSchedulers(projects))
    s.extend(createNightlySchedulers(projects))
    s.append(createRegularScheduler(config['builders']))
    s.append(createReleaseScheduler(config['builders']))

