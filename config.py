import os, socket, getpass

artifacts_path = os.path.expanduser('~/public_html/artifacts')
artifacts_baseurl = 'http://{0}/~{1}/artifacts'.format(socket.getfqdn(), getpass.getuser())

nightly_starthour = 3
nightly_interval = 15

