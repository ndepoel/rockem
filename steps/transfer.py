from buildbot.steps.transfer import MultipleFileUpload
from buildbot.process import properties, remotecommand

from twisted.internet import defer
from twisted.python import log

import os

class GlobFileUpload(MultipleFileUpload):
    def __init__(self, glob_path, masterdest, *args, **kwargs):
        try:
            self.url_name = kwargs.pop('url_name')
        except:
            self.url_name = None
    
        MultipleFileUpload.__init__(self, [], masterdest, *args, **kwargs)
        self.glob_path = glob_path

    def start(self):
        @defer.inlineCallbacks
        def runGlob():
            cmd = remotecommand.RemoteCommand('glob', {'path': self.glob_path})
            yield self.runCommand(cmd)

            glob_result = cmd.updates['files'][-1]
            log.msg('Result of glob: {0}'.format(glob_result))
            self.slavesrcs = [fn.replace("\\", "/") for fn in glob_result]
            
        d = runGlob()
        d.addCallback(lambda result: MultipleFileUpload.start(self))
        d.addErrback(self.failed)
        return d

    def allUploadsDone(self, result, sources, masterdest):
        if self.url is not None:
            self.addURL(self.url_name or os.path.basename(masterdest), self.url)

