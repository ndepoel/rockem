from buildbot.steps import shell

class XBuild(shell.WarningCountingShellCommand):
    renderables = ['installdir']

    def __init__(self,
                 target = None, config = None, properties = None, projectfile = None,
                 installdir = None, **kwargs):
        
        self.installdir = installdir
        
        self.target = target
        self.config = config
        self.properties = properties
        self.projectfile = projectfile
        
        shell.WarningCountingShellCommand.__init__(self, **kwargs)
        
    def setupLogfiles(self, cmd, logfiles):
        logfiles['msbuild'] = 'msbuild.log'
        shell.WarningCountingShellCommand.setupLogfiles(self, cmd, logfiles)
        
    def getInstalldir(self, os):
        if self.installdir:
            return self.installdir
            
        if os == 'windows':
            return 'C:\Program Files (x86)\MSBuild\\12.0\Bin'
        elif os == 'osx':
            return '/usr/bin'
            
        raise ValueError('Unsupported OS: {0}'.format(os))

    def start(self):
        os = self.getProperty('os')
        if os == 'windows':
            command = [self.getInstalldir(os) + '\MSBuild.exe']
        elif os == 'osx':
            command = [self.getInstalldir(os) + '/xbuild']
        else:
            raise ValueError('Unsupported OS: {0}'.format(os))
            
        command.extend(['/flp:LogFile=msbuild.log', '/target:{0}'.format(self.target)])
        if self.config:
            command.append('/property:Configuration={0}'.format(self.config))
            
        if self.properties:
            props = self.getProperties()
            for key in self.properties:
                val = self.properties[key]
                if hasattr(val, '__call__'):
                    val = val(props)
                
                command.append('/property:{0}={1}'.format(key, val))
            
        command.append(self.projectfile)
        self.setCommand(command)
        return shell.WarningCountingShellCommand.start(self)

