from buildbot.steps import shell

class Unity(shell.WarningCountingShellCommand):
    PlayerTypes = {
        'win32': 'WindowsPlayer', 
        'win64': 'Windows64Player', 
        'osx': 'OSXPlayer', 
        'osxintel64': 'OSX64Player', 
        'osxuniversal': 'OSXUniversalPlayer', 
        'linux': 'Linux32Player', 
        'linux64': 'Linux64Player', 
        'linuxuniversal': 'LinuxUniversalPlayer', 
        'web': 'WebPlayer', 
        'webstreamed': 'WebPlayerStreamed',
    }

    renderables = ['installdir']

    def __init__(self,
                 buildTarget = None, buildPlayer = None, executeMethod = None,
                 installdir = None, extraLogfiles = None, **kwargs):
        
        if buildPlayer and not buildPlayer in Unity.PlayerTypes:
            raise ValueError('Unsupported Unity Player type: {0}'.format(buildPlayer))
        
        self.installdir = installdir
        self.extraLogfiles = extraLogfiles
        
        self.buildTarget = buildTarget
        self.buildPlayer = Unity.PlayerTypes.get(buildPlayer)
        self.executeMethod = executeMethod
        
        shell.WarningCountingShellCommand.__init__(self, **kwargs)
        
    def setupLogfiles(self, cmd, logfiles):
        logfiles['UnityLog'] = 'UnityBuild.log'
        if self.extraLogfiles:
            logfiles.update(self.extraLogfiles)
        
        shell.WarningCountingShellCommand.setupLogfiles(self, cmd, logfiles)
        
    def getInstallDir(self, os):
        if self.installdir:
            return self.installdir
            
        if os == 'windows':
            return 'C:\Program Files\Unity'
        elif os == 'osx':
            return '/Applications/Unity'
            
        raise ValueError('Unsupported OS: {0}'.format(os))
        
    def start(self):
        os = self.getProperty('os')
        if os == 'windows':
            command = [self.getInstallDir(os) + '\Editor\Unity.exe']
        elif os == 'osx':
            command = [self.getInstallDir(os) + '/Unity.app/Contents/MacOS/Unity']
        else:
            raise ValueError('Unsupported OS: {0}'.format(os))
            
        workdir = self.getProperty('workdir')
        command.extend(['-batchmode', '-nographics', '-silent-crashes', '-quit'])
        command.extend(['-projectPath', '{0}/build'.format(workdir)])
        command.extend(['-logFile', '{0}/build/UnityBuild.log'.format(workdir)])
        
        if self.buildTarget:
            command.extend(['-buildTarget', self.buildTarget])
        
        if self.buildPlayer:
            command.extend(['-build{0}'.format(self.buildPlayer),
                'Build/{0}'.format(self.buildPlayer)])
        
        if self.executeMethod:
            command.extend(['-executeMethod', self.executeMethod])
            
        self.setCommand(command)
        return shell.WarningCountingShellCommand.start(self)

