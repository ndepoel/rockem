def setupSlaves(config, slaves):
    if not 'slaves' in config:
        config['slaves'] = []

    for slave in slaves:
        print('Registering build slave: {0}'.format(slave))
        config['slaves'].append(slave.createBuildslave())

